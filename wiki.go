package main

import (
	"flag"

	"bitbucket.org/saj1th/minipedia/app"
	"bitbucket.org/saj1th/minipedia/handlers"
	"bitbucket.org/saj1th/minipedia/middleware"
	"bitbucket.org/saj1th/minipedia/services"
	"github.com/Sirupsen/logrus"
	"github.com/goji/glogrus"
	"github.com/saj1th/envtoflag"
	"github.com/zenazn/goji"
	gmiddleware "github.com/zenazn/goji/web/middleware"
)

var (
	logr    *logrus.Logger
	appName string
)

func init() {
	logr = logrus.New()
	logr.Formatter = new(logrus.JSONFormatter)

	appName = "minipedia"

	goji.Abandon(gmiddleware.Logger)             //Remove default logger
	goji.Abandon(gmiddleware.Recoverer)          //Remove default Recoverer
	goji.Use(middleware.RequestIDHeader)         //Add RequestIDHeader Middleware
	glogrus := glogrus.NewGlogrus(logr, appName) //Add custom logger Middleware
	goji.Use(glogrus)
	goji.Use(middleware.NewRecoverer(logr)) //Add custom recoverer

}

func main() {

	var (
		mode       string
		redis_host string
		data_path  string
		cache_ttl  int
		config     *app.Config
		errors     []string
	)
	flag.StringVar(&mode, "mode", "dev", "dev|debug|prod")
	flag.StringVar(&cache_ttl, "cache-url", "1", "cache ttl in seconds")
	flag.StringVar(&redis_host, "redis-host", "", "redis.host")
	flag.StringVar(&data_path, "data-path", "", "FS path where wiki files are stored")
	envtoflag.Parse(appName)

	if redis_host == "" {
		errors = append(errors, "redis-host")
	}
	if cache_ttl == "" {
		errors = append(errors, "cache-ttl")
	}
	if data_path == "" {
		errors = append(errors, "data-path")
	}

	app.PrintWelcome()
	app.ParseErrors(errors)
	logr.Level = app.GetLogrMode(mode)

	config = app.NewConfig(cache_ttl, data_path)

	redis := services.NewRedis(redis_host, app.REDIS_MAX_IDLE, app.REDIS_MAX_ACTIVE)

	bH := handlers.NewBaseHandler(logr, config)
	wikiH := handlers.NewWikiHandler(bH, redis)

	goji.Get("/wiki/:id", wikiH.ShowWiki)
	goji.Put("/wiki/:id", wikiH.UpdateWiki)

	goji.Serve()
}
