package handlers

import (
	"net/http"
	"encoding/json"

	"bitbucket.org/saj1th/minipedia/app"
	"github.com/Sirupsen/logrus"
	"github.com/zenazn/goji/web"
	gmiddleware "github.com/zenazn/goji/web/middleware"
)

//AppH is the signature of our handlers. In addition to goji's handler type
//AppH returns an app.Err
type AppH func(web.C, http.ResponseWriter, *http.Request) *app.Err

// BaseHandler is the base handler. Other handlers embeds this one
type BaseHandler struct {
	Logr      *logrus.Logger
	Config    *app.Config
}

//NewBaseHandler is the BaseHandler constructor
func NewBaseHandler(l *logrus.Logger, c *app.Config) *BaseHandler {
	return &BaseHandler{Logr: l, Config: c}
}

// Respond writes an HTTP response to the given resp,
func (b *BaseHandler) Respond(w http.ResponseWriter, status int, data string) {
	d, err := json.MarshalIndent(data, "", "  ")
	if nil != err {
		b.Logr.Error(err)
	}
	w.WriteHeader(status)
	_, err = w.Write(d)
	if nil != err {
		b.Logr.Errorf("web.ioerror %s", err.Error())
	}
	_, err = w.Write([]byte("\n"))
	if nil != err {
		b.Logr.Errorf("web.ioerror %s", err.Error())
	}
}

//Render renders the template
func (b *BaseHandler) Render(w http.ResponseWriter, name, template string, data interface{}) error {
	return nil
}


//Route is used because our handlers returns *app.Err
func (b *BaseHandler) Route(h AppH) func(web.C, http.ResponseWriter, *http.Request) {
	fn := func(c web.C, w http.ResponseWriter, r *http.Request) {
		err := h(c, w, r)
		if err != nil {
			reqID := gmiddleware.GetReqID(c)
			b.Logr.WithFields(logrus.Fields{
				"req_id": reqID,
				"err":    err.Error(),
			}).Error("response.err")

			http.Error(w, http.StatusText(err.HTTPStatus), err.HTTPStatus)
		}
	}
	return fn
}

//NotFound
func (b *BaseHandler) NotFound(c web.C, w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusNotFound)
}
