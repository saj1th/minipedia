package handlers

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/saj1th/minipedia/models"
	"bitbucket.org/saj1th/minipedia/services"
	"github.com/rcrowley/go-metrics"
	"github.com/zenazn/goji/web"
	"sync"
)

//WikiHandler - home for all wiki operations handlers
type WikiHandler struct {
	*BaseHandler
	RD          *services.Redis
	Mu          sync.Mutex
	ReadMetric  metrics.Counter
	WriteMetric metrics.Counter
}

func NewWikiHandler(bh *BaseHandler, redis *services.Redis) *WikiHandler {
	reads := metrics.NewCounter()
	writes := metrics.NewCounter()
	metrics.Register("wiki.reads", reads)
	metrics.Register("wiki.writes", writes)

	return &WikiHandler{BaseHandler: bh, RD: redis, ReadMetric:reads, WriteMetric:writes}
}

//ShowWiki shows the wiki page
func (w *WikiHandler) ShowWiki(c web.C, w http.ResponseWriter, r *http.Request) {
	w.ReadMetric.Inc(1)
	wikiID := c.URLParams["id"]
	content, msg := models.FetchWiki(wikiID, w)
	if msg != nil {
		w.Respond(w, 404, msg)
	} else {
		w.Respond(w, 200, content)
	}

}

//UpdateWiki updates the wiki page
// Use mutex or channels to serialize access
func (w *WikiHandler) UpdateWiki(c web.C, w http.ResponseWriter, r *http.Request) {
	w.WriteMetric.Inc(1)
	decoder := json.NewDecoder(r.Body)
	var wiki models.Wiki
	err := decoder.Decode(&wiki)
	if err != nil {
		w.Respond(w, 400, "Invalid request parameters")
	}
	err = models.UpdateWiki(wiki, w)
	if err != nil {
		w.Respond(w, 500, "Internal server error")
	}

	return nil
}
