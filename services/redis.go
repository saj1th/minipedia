package services

import (
	"github.com/garyburd/redigo/redis"

)

// Redis adapts redigo pool to
// minipedia data store service
type Redis struct {
	Pool *redis.Pool
}

// NewRedis returns minipedia redis service
func NewRedis(redisHost string, maxIdle, maxActive int) *Redis {
	pool := &redis.Pool{
		MaxIdle: maxIdle,
		MaxActive: maxActive, // max number of connections
		Dial: func() (redis.Conn, error) {
			c, err := redis.Dial("tcp", redisHost)
			if err != nil {
				panic(err.Error())
			}
			return c, err
		},
	}

	return &Redis{Pool:pool}
}

