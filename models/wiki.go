package models

import (
	"bitbucket.org/saj1th/minipedia/app"
	"bitbucket.org/saj1th/minipedia/handlers"
	"fmt"
	"github.com/garyburd/redigo/redis"
	"io/ioutil"
)

//Wiki model
type Wiki struct {
	ID   string
	Contents string
	MetaData
}

func (w *Wiki) String() string {
	return fmt.Sprintf("ID:%s  MetaData:%s", w.ID,  w.MetaData)
}

func FetchWiki(ID string, w *handlers.WikiHandler) (*Wiki, *app.Msg) {
	//Fetch the connection from pool and defer close
	c := w.RD.Pool.Get()
	defer c.Close()

	reply, err := c.Do("GET", ID)
	contents, err := redis.String(reply, err)
	if err != nil {
		//Fetch file from disk and persist it in cache
		data, err := fetchWikiFromDisk(ID, w.Config.DataPath)
		if err != nil {
			//Do error logging
			w.Logr.Error(err)
			return nil, app.NewErrMsg("Unable to fetch the wiki")
		}
		contents = string(data)
		// Persist it to Redis
		c.Do("SET", ID, contents)
		c.Do("EXPIRE", ID, w.Config.TTL)
	}

	return &Wiki{ID: ID, Contents: contents}
}

func fetchWikiFromDisk(ID, wikiPath string) ([]byte, error) {
	return ioutil.ReadFile(wikiPath + "/" + ID)
}

func UpdateWiki(wiki Wiki, w *handlers.WikiHandler) *app.Msg{

	// Check the revisions table to see if there is a revision match
	// If not return error
	//If revision matches, lock the mutex, update the file and unlock it

	w.Mu.Lock()
	defer w.Mu.Unlock()
	err := ioutil.WriteFile(w.Config.DataPath +"/"+ wiki.ID, []byte(wiki.Contents), 0755)
	if err != nil {
		w.Mu.Unlock()
		//Do error logging
		w.Logr.Error(err)
		return  app.NewErrMsg("Unable to write the wiki")
	}
	w.Mu.Unlock()
	return nil
}
