package models
import "fmt"



//WikiMetaData model
type MetaData struct {
	Author       string
	CurrentRevision string
}

func (m *MetaData) String() string {
	return fmt.Sprintf("Author:%s CurrentRevision:%s", m.Author, m.CurrentRevision)
}
