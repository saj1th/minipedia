package app

import (
	"fmt"
)

// Envs
const (
	MODE_DEV         string = "dev"
	MODE_PROD        string = "prod"
	MODE_DEBUG       string = "debug"
	REDIS_MAX_IDLE   int    = 80
	REDIS_MAX_ACTIVE int    = 1200
)

type Config struct {
	TTL      int
	DataPath string
}

func (c Config) String() string {
	return fmt.Sprintf("TTL:%s , Path:%s", c.TTL, c.DataPath)
}

//NewConfig creates a config object
func NewConfig(ttl int, path string) *Config {
	return &Config{TTL: ttl, DataPath: string()}
}
